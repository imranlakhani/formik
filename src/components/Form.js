import 'date-fns';
import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  Container,
  Button,
  FormControl,
  Select,
  MenuItem,
  Input,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  TextField,
} from '@material-ui/core';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { Autocomplete } from '@material-ui/lab';

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];

const defaultProps = {
  options: names,
  getOptionLabel: (option) => option,
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const initialValues = {
  selected: [],
  dob: null,
  gender: '',
  autocomplete: '',
};

const validationSchema = Yup.object({
  dob: Yup.date().nullable().required('Required Field'),
  selected: Yup.array()
    .required('Required Field')
    .min(1, 'Please select atleast 1 value'),
  gender: Yup.string().required('Required Field'),
  autocomplete: Yup.string().required('Required Field'),
});

const onSubmit = (values) => {
  console.log(values);
};

const LoginForm = () => {
  const formik = useFormik({ initialValues, onSubmit, validationSchema });

  return (
    <Container maxWidth="sm">
      <form onSubmit={formik.handleSubmit}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            id="date-picker-dialog"
            label="Date picker dialog"
            inputVariant="standard"
            format="MM/dd/yyyy"
            clearable
            invalidDateMessage="Required Field"
            value={formik.values.dob}
            onChange={(value) => formik.setFieldValue('dob', value)}
            onBlur={($event) => {
              console.log($event.target.value);
              if ($event.target.value.length < 1) {
                formik.setFieldValue('dob', null);
                formik.setFieldTouched('dob', true);
                formik.setFieldError('dob', 'Required Field !');
              }
            }}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
        </MuiPickersUtilsProvider>
        {formik.touched.dob && formik.errors.dob ? (
          <div className="error">{formik.errors.dob}</div>
        ) : null}
        <FormControl>
          <Select
            multiple
            displayEmpty
            value={formik.values.selected}
            onChange={formik.handleChange}
            input={<Input name="selected" />}
            renderValue={(selected) => {
              if (selected.length === 0) {
                return <em>Placeholder</em>;
              }

              return selected.join(', ');
            }}
            MenuProps={MenuProps}
            inputProps={{ 'aria-label': 'Without label' }}
          >
            <MenuItem disabled value="">
              <em>Placeholder</em>
            </MenuItem>
            {names.map((name) => (
              <MenuItem key={name} value={name}>
                {name}
              </MenuItem>
            ))}
          </Select>
          {formik.touched.selected && formik.errors.selected ? (
            <div className="error">{formik.errors.selected}</div>
          ) : null}
        </FormControl>
        <FormControl component="fieldset">
          <FormLabel component="legend">Gender</FormLabel>
          <RadioGroup
            aria-label="gender"
            name="gender"
            defaultValue={formik.initialValues.gender}
          >
            <FormControlLabel
              value="female"
              control={<Radio />}
              label="Female"
              onChange={formik.handleChange}
            />
            <FormControlLabel
              value="male"
              control={<Radio />}
              label="Male"
              onChange={formik.handleChange}
            />
            <FormControlLabel
              value="other"
              control={<Radio />}
              label="Other"
              onChange={formik.handleChange}
            />
          </RadioGroup>
          {formik.touched.gender && formik.errors.gender ? (
            <div className="error">{formik.errors.gender}</div>
          ) : null}
        </FormControl>
        <Autocomplete
          {...defaultProps}
          id="auto-complete"
          name="autocomplete"
          autoComplete
          multiple
          includeInputInList
          autoHighlight
          filterSelectedOptions
          onChange={($event, value, reason) => {
            console.log(`On Change triggered: ${value}`);
            formik.setFieldValue('autocomplete', value);
          }}
          onClose={($event, reason) => {
            if (reason === 'blur' || reason === 'escape') {
              formik.setFieldValue('autocomplete', $event.target.value);
              formik.setFieldTouched('autocomplete', true);
            }
          }}
          renderInput={(params) => (
            <TextField {...params} label="autoComplete" margin="normal" />
          )}
        />
        {formik.touched.autocomplete && formik.errors.autocomplete ? (
          <div className="error">{formik.errors.autocomplete}</div>
        ) : null}
        <Button type="submit">Submit</Button>
      </form>
    </Container>
  );
};

export default LoginForm;
